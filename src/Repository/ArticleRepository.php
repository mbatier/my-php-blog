<?php

namespace App\Repository;

use App\Entity\Article;



class ArticleRepository
{

    public function getAll() : array
    {
        $articles = [];
        try {
            $cnx = new \PDO(
                "mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"],
                $_ENV["MYSQL_USER"],
                $_ENV["MYSQL_PASSWORD"]
            ); 
        
        //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp"); (ça correspond à ça sans concaténation)
            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        //on indique à PDO de déclencher des exceptions s'il a une erreur
            $query = $cnx->prepare("SELECT * FROM article");
            $query->execute();

            foreach ($query->fetchAll() as $row) {
                $article = new Article();
                $article->fromSQL($row);
                $articles[] = $article;
            }

        } catch (\PDOException $e) {
            dump($e);
        }
        return $articles;
    }



    public function add(Article $article)
    {
        $articles = [];
        try {
            $cnx = new \PDO(
                "mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"],
                $_ENV["MYSQL_USER"],
                $_ENV["MYSQL_PASSWORD"]
            ); 
        
        //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp"); (ça correspond à ça sans concaténation)
            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);


            $query = $cnx->prepare("INSERT INTO article (content, view, date, resume, title) VALUES (:content, :view, :date, :resume, :title)");
            $query->bindValue(":content", $article->content);
            $query->bindValue(":view", $article->view);
            $query->bindValue(":date", $article->date);
            $query->bindValue(":resume", $article->resume);
            $query->bindValue(":title", $article->title);

            $query->execute();

            $article->id = intval($cnx->lastInsertId());

        } catch (\PDOException $e) {
            dump($e);
        }
        return $articles;
    }

    public function update(Article $article)
    {
        $articles = [];
        try {
            $cnx = new \PDO(
                "mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"],
                $_ENV["MYSQL_USER"],
                $_ENV["MYSQL_PASSWORD"]
            ); 
        
        //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp"); (ça correspond à ça sans concaténation)
            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);


            $query = $cnx->prepare("UPDATE article SET title=:title, content=:content, resume=:resume WHERE id=:id");
            $query->bindValue(":title", $article->title);
            
            $query->bindValue(":content", $article->content);
            // $query->bindValue(":view", $article->view);
            // $query->bindValue(":date", $article->date);
            $query->bindValue(":resume", $article->resume);
            $query->bindValue(":id", $article->id);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }


    public function delete(int $id)
    {
        $articles = [];
        try {
            $cnx = new \PDO(
                "mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"],
                $_ENV["MYSQL_USER"],
                $_ENV["MYSQL_PASSWORD"]
            ); 
        
        //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp"); (ça correspond à ça sans concaténation)
            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);


            $query = $cnx->prepare("DELETE FROM article WHERE id=:id");

            $query->bindValue(":id", $id);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }


    public function getById(int $id = null) : ? Article
    {
        $articles = [];
        try {
            $cnx = new \PDO(
                "mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"],
                $_ENV["MYSQL_USER"],
                $_ENV["MYSQL_PASSWORD"]
            ); 
        
        //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp"); (ça correspond à ça sans concaténation)
            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);


            $query = $cnx->prepare("SELECT * FROM article WHERE id=:id");

            $query->bindValue(":id", $id);

            $query->execute();
            $result = $query->fetchAll();

            if (count($result) === 1) {
                $article = new Article();
                $article->fromSQL($result[0]);
                return $article;
            }

        } catch (\PDOException $e) {
            dump($e);
        }
        return null;
    }
}