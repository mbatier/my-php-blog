<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ArticleType;


class AddArticleController extends Controller
{
    /**
     * @Route("/addArticle", name="add")
     */
    public function index(ArticleRepository $repo, Request $request)
    {
        $user = new Article;

        $form = $this->createForm(ArticleType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
                // perform some action...
            $test = $form->getData();
            $repo->add($test);
            return $this->redirectToRoute("homepage");
        }


        return $this->render('addArticle.html.twig', [
            'form' => $form->createView()
        ]);
    }
    }
