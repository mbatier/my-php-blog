<?php

namespace App\Entity;

class Article {
    public $title;
    public $view;//
    //public $user_comment;
    //public $biopic;
    //public $nb_comment;
    public $date;//
    public $content;//
    //public $image;
    public $resume;//
    public $id;//

    public function fromSQL(array $sql) {
        $this->id = $sql["id"];
        $this->title = $sql["title"];
        $this->content = $sql["content"];
        $this->view = $sql["view"];
        $this->date = $sql["date"];
        $this->resume = $sql["resume"];
    }
}