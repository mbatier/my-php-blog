<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;

class ArticleController extends Controller
{
    /**
     * @Route("/article/{id}", name="article")
     */
    public function index(int $id, ArticleRepository $repo)
    {
        $id = $repo->getById($id);
        return $this->render('article.html.twig', [
            'article' => $id
        ]);
        
    }
}
