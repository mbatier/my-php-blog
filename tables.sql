CREATE DATABASE  IF NOT EXISTS `db_blog` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_blog`;
-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db_blog
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `view` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `resume` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,'\nAs one of Europe s fastest growing cities this decade, Oslo is buzzing with energy from new neighbourhoods and cutting-edge food, to fashion and art scenes.',6,'2018-12-24 00:00:00','Capital of Norway','Oslo'),(2,'Madrid is a city so full of life and culture that it s hard to do justice to it in a few paragraphs.',45,'2018-06-13 00:00:00','Capital of Spain','Madrid'),(3,'Seoul s global reputation centers on its rapid pace, technological prowess and glittering skyscrapers.',15,'2018-07-22 00:00:00','Capital of South Korea','Seoul'),(4,'Washington DC is not one of the 50 states. But it s an important part of the U.S. The District of Columbia is our nation s capital.',150,'2018-01-30 00:00:00','Capital of United States','Washington DC'),(5,'Moscow, Russian Moskva, city, capital of Russia, located in the far western part of the country.',67,'2018-07-17 00:00:00','Capital of Russia','Moscow'),(6,'The history of the city of Tokyo stretches back some 400 years. Originally named Edo, the city started to flourish after Tokugawa Ieyasu established the Tokugawa Shogunate here in 1603.',32,'2018-02-04 00:00:00','Capital of Japan','Tokyo'),(7,'From the fifteenth century onwards, the port of Lisbon became one of the most important in the world.',94,'2018-05-24 00:00:00','Capital of Portugal','Lisbon'),(14,'fc bxcfbnfdfc',NULL,NULL,'bdxvcx','ok');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-25 16:17:19
