<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;



class SingleArticleController extends Controller
{
    /**
     * @Route("/onearticle/{id}", name="onearticle")
     */
    public function index(int $id, ArticleRepository $repo, Request $request)
    {
        $article = $repo->getById($id);

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
             $repo->update($form->getData());
             return $this->redirectToRoute("homepage");
        }

        return $this->render('single_article/index.html.twig', [
            'form' => $form->createView(),
            "article" => $article
        ]);
    }



}
