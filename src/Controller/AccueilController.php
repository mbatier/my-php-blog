<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;



class AccueilController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */
    public function index(ArticleRepository $repo)//injecter repository
    {
        //getAll
        $all = $repo->getAll();
        return $this->render("accueil.html.twig", [ 'tab_articles' => $all]);//exposer dans template
    }



}