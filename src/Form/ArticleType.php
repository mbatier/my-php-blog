<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('attr' => array('placeholder' => 'Title'), 'label' => false))
            ->add('resume', TextType::class, array('attr' => array('placeholder' => 'Resume'), 'label' => false))
            ->add('content', TextareaType::class, array('attr' => array('placeholder' => 'Content'), 'label' => false));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        
        $resolver->setDefaults([
            "data_class" => Article::class
        ]);
    }
}